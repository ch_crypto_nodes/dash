# Dash

A Dash docker image.

## Tags

- `0.17.0.3` ([0.17.0.3/GitHub](https://github.com/dashpay/dash/tree/v0.17.0.3))
- `0.17.0.2` ([0.17.0.2/GitHub](https://github.com/dashpay/dash/tree/v0.17.0.2))
- `0.15.0.0` ([0.15/GitHub](https://github.com/dashpay/dash/tree/v0.15.0.0))

## What is Dash?

Dash is an experimental digital currency that enables anonymous, instant
payments to anyone, anywhere in the world. Dash uses peer-to-peer technology
to operate with no central authority: managing transactions and issuing money
are carried out collectively by the network. Dash Core is the name of the open
source software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the Dash Core software, see https://www.dash.org/get-dash/.

## Usage

### How to use this image

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `RPC_USER_NAME`                 | User to secure the JSON-RPC api.                                           |
| `RPC_USER_PASSWORD`             | A password to secure the JSON-RPC api.                                     | 
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |


Run container: 

```bash
docker-compose up -d 

```

Stop container: 

```bash
docker-compose down 

```
